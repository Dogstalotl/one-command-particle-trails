# One-Command-Particle-Trails
I took 3 hours out of my time to code this, Pray to me!

Source Code:

INIT: /tellraw @p ["",{"text":"Click here for a link to the give code of the selector book","color":"blue","underlined":"true","clickEvent":{"action":"open_url","value":"https://github.com/TheMaxShaft/One-Command-Particle-Trails/blob/master/Book%20give%20code.md"}}]

INIT: /tellraw @p ["",{"text":"Command By: TheMaxShaft™","color":"dark_aqua"}]

INIT: /scoreboard objectives add pexplode dummy

INIT: /scoreboard objectives add plexplode dummy

INIT: /scoreboard objectives add phexplode dummy

INIT: /scoreboard objectives add pfsparks dummy

INIT: /scoreboard objectives add psplash dummy

INIT: /scoreboard objectives add pcrit dummy

INIT: /scoreboard objectives add pmcrit dummy

INIT: /scoreboard objectives add psmoke dummy

INIT: /scoreboard objectives add plsmoke dummy

INIT: /scoreboard objectives add pspell dummy

INIT: /scoreboard objectives add pcloud dummy

INIT: /scoreboard objectives add pheart dummy

INIT: /scoreboard objectives add preddust dummy

INIT: /scoreboard objectives add petable dummy

INIT: /scoreboard objectives add pportal dummy

INIT: /scoreboard objectives add pnote dummy

INIT: /scoreboard objectives add phappyvillager dummy

INIT: /scoreboard objectives add pangryvillager dummy

INIT: /scoreboard objectives add pmspella dummy

INIT: /scoreboard objectives add pmspell dummy

/execute @a[score_pcloud_min=1] ~ ~ ~ particle cloud ~ ~1.5 ~ 1 1 1 1 10

/execute @a[score_pheart_min=1] ~ ~ ~ particle heart ~ ~3 ~ 1 1 1 0 1

/execute @a[score_preddust_min=1] ~ ~ ~ particle reddust ~ ~2 ~ 1 1 1 19 10

/execute @a[score_petable_min=1] ~ ~ ~ particle enchantmenttable ~ ~0.5 ~ 1 1 1 1 50

/execute @a[score_pportal_min=1] ~ ~ ~ particle portal ~ ~0.5 ~ 1 1 1 0 50

/execute @a[score_pnote_min=1] ~ ~ ~ particle note ~ ~3 ~ 1 1 1 100 1

/execute @a[score_phappyvillager_min=1] ~ ~ ~ particle happyVillager ~ ~3 ~ 1 1 1 100 1

/execute @a[score_pangryvillager_min=1] ~ ~ ~ particle angryVillager ~ ~3 ~ 1 1 1 100 1

/execute @a[score_pmspella_min=1] ~ ~ ~ particle mobSpellAmbient ~ ~ ~ 1 1 1 100 10

/execute @a[score_pmspell_min=1] ~ ~ ~ particle mobSpell ~ ~ ~ 1 1 1 100 10

/execute @a[score_pspell_min=1] ~ ~ ~ particle spell ~ ~ ~ 1 1 1 0 10 force

/execute @a[score_plsmoke_min=1] ~ ~ ~ particle largesmoke ~ ~1 ~ 1 1 1 0 1 force

/execute @a[score_psmoke_min=1] ~ ~ ~ particle smoke ~ ~1 ~ 1 1 1 0 10 force

/execute @a[score_pmcrit_min=1] ~ ~ ~ particle magicCrit ~ ~0.5 ~ 1 1 1 0 10 force

/execute @a[score_pcrit_min=1] ~ ~ ~ particle crit ~ ~0.5 ~ 1 1 1 0 10 force

/execute @a[score_psplash_min=1] ~ ~ ~ particle splash ~ ~0.5 ~ 1 1 1 0 100 force

/execute @a[score_pfsparks_min=1] ~ ~ ~ particle fireworksSpark ~ ~0.5 ~ 1 1 1 0 1 force

/execute @a[score_phexplode_min=1] ~ ~ ~ particle hugeexplosion ~ ~0.5 ~ 1 1 1 0 1 force

/execute @a[score_plexplode_min=1] ~ ~ ~ particle largeexplode ~ ~0.5 ~ 1 1 1 0 1 force

/execute @a[score_pexplode_min=1] ~ ~ ~ particle explode ~ ~0.5 ~ 1 1 1 1 10 force
